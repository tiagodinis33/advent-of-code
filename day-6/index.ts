let gameState: number[] = new Array(9).fill(0)
function convertInitialState(arr: number[]){
    arr.forEach(timer => {
        gameState[timer]++
    });
}
convertInitialState([1,4,1,1,1,1,5,1,1,5,1,4,2,5,1,2,3,1,1,1,1,5,4,2,1,1,3,1,1,1,1,1,1,1,2,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,4,1,1,1,1,5,1,4,1,1,4,1,1,1,1,4,1,1,5,5,1,1,1,4,1,1,1,1,1,3,2,1,1,1,1,1,2,3,1,1,2,1,1,1,3,1,1,1,2,1,2,1,1,2,1,1,3,1,1,1,3,3,5,1,4,1,1,5,1,1,4,1,5,3,3,5,1,1,1,4,1,1,1,1,1,1,5,5,1,1,4,1,2,1,1,1,1,2,2,2,1,1,2,2,4,1,1,1,1,3,1,2,3,4,1,1,1,4,4,1,1,1,1,1,1,1,4,2,5,2,1,1,4,1,1,5,1,1,5,1,5,5,1,3,5,1,1,5,1,1,2,2,1,1,1,1,1,1,1,4,3,1,1,4,1,4,1,1,1,1,4,1,4,4,4,3,1,1,3,2,1,1,1,1,1,1,1,4,1,3,1,1,1,1,1,1,1,5,2,4,2,1,4,4,1,5,1,1,3,1,3,1,1,1,1,1,4,2,3,2,1,1,2,1,5,2,1,1,4,1,4,1,1,1,4,4,1,1,1,1,1,1,4,1,1,1,2,1,1,2])
function range(from: number, to: number): number[] {
    if (from > to) {
        return []
    }
    return new Array(to - from).fill(0).map((_, it) => it + from)
}
function stepGame() {
    let localGameState = new Array(9).fill(0)
    localGameState[8] += gameState[0]
    localGameState[6] += gameState[0]
    for (const n of range(1,9)) {
        localGameState[n-1]+=gameState[n]
    }
    gameState = localGameState
}
const part2 = true
const num = part2 ? 256 : 80
for (let i = 0; i < num; i++) {
    console.log(i)
    stepGame()
}
console.log(gameState.reduce((last, current)=> last + current))
