import { readFile } from "fs/promises"
import assert from "assert"
const data = readFile("day-3/input.txt", "utf-8").then(main)
function main(data: string) {
    let bitCount1 = 0
    let bitCount0 = 0
    function gammaRate() {
        return Number.parseInt(gammaRateStr, 2)
    }
    let gammaRateStr = ""
    function epsilonRate(): number {

        let str = gammaRateStr
        return Number.parseInt(str.split('').map(b => (1 - Number.parseInt(b, 2)).toString()).join(''), 2)
    }
    function result(): number {
        return gammaRate() * epsilonRate()
    }
    let lines = data.split("\n").filter(str => str.length > 0)
    // This program assumes that all lines have the same size
    // But its uneficient to assert so i'll skip
    const linesSize = lines[0].length
    for (let col = 0; col < linesSize; col++) {
        for (let line = 0; line < lines.length; line++) {
            const lineStr = lines[line];
            const char = lineStr[col]
            switch (char) {
                case "0":
                    bitCount0++
                    break;
                case "1":
                    bitCount1++
                    break;
                default:
                    assert(false, "Invalid char: " + char)
                    break;
            }
        }
        gammaRateStr += bitCount1 > bitCount0 ? "1" : "0"

        bitCount1 = 0
        bitCount0 = 0
    }
    console.log("Gamma rate:")
    console.log(gammaRate())
    console.log("Epsilon Rate:")
    console.log(epsilonRate())
    console.log("Result:")
    console.log(result())
}