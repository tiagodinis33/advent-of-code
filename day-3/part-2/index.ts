import { readFile } from "fs/promises"
import assert from "assert"
const data = readFile("day-3/input.txt", "utf-8").then(main)
function main(data: string) {
    let fileLines = data.split("\n").filter(str => str)
    function getCount(lines: string[], col: number) {
        let white = 0;
        let black = 0;
        for (let line = 0; line < lines.length; line++) {
            const lineStr = lines[line];
            const char = lineStr[col]
            switch (char) {
                case "0":
                    black++
                    break;
                case "1":
                    white++
                    break;
                default:
                    assert(false, "Invalid char: " + char)
                    break;
            }
        }
        return { white, black }
    }

    function getCO2(lines: string[] = fileLines, index = 0): string {
        assert(lines.length)
        let { black, white } = getCount(lines, index);
        let commonBit = "0";
        if(white < black )
            commonBit = "1"

        const filtered = lines.filter((str) => {
            return str[index] == commonBit
        })

        console.log(lines)
        if (filtered.length == 1) {
            return filtered[0]
        }
        return getCO2(filtered, index + 1);
    }
    function getOxygen(lines: string[] = fileLines, index = 0): string {
        assert(lines.length)
        let { black, white } = getCount(lines, index);
        let commonBit = "0";
        if(white >= black )
            commonBit = "1"

        const filtered = lines.filter((str) => {
            return str[index] == commonBit
        })

        console.log(lines)
        if (filtered.length == 1) {
            return filtered[0]
        }
        return getOxygen(filtered, index + 1);
    }
    let result = Number.parseInt(getOxygen(), 2) * Number.parseInt(getCO2(), 2);
    console.log(result)
}