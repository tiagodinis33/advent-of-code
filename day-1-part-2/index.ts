import { readFileSync } from "fs";
const data = readFileSync("day-1-part-1/report.txt", "utf-8")

let lines = data.split("\n");
let lastSum = 0;
let result = -1;
for(var i = 0; i < lines.length; i++){
    const num1 = parseInt(lines[i]);
    const num2 = parseInt(lines[i+1]);
    const num3 = parseInt(lines[i+2]);
    if(Number.isNaN(num2) || Number.isNaN(num3)){
        break;
    }
    const sum = num1 + num2 + num3;
    console.log(sum)
    if(sum > lastSum && lastSum != -1){
        result++;
    }
    lastSum = sum
}
console.log("Count: "+ result);