import { readFile } from "fs/promises";
import Vector from "victor"
declare global {
    interface Number {
        pow(exponent: number): number
        roundToInt(): number
    }
}
interface Line {
    start: Vector,
    end: Vector
}
Number.prototype.roundToInt = function (this: number) {
    return Math.round(this)
}
Number.prototype.pow = function (this: number, exponent: number) {
    return Math.pow(this, exponent)
}
function range(from: number, to: number): number[] {
    if (from > to) {
        return []
    }
    return new Array(to - from).fill(0).map((_, it) => it + from)
}
function isDiagonal(line: Line){
    return !(line.start.x == line.end.x || line.start.y == line.end.y)
}
function hops(line: Line){
    return isDiagonal(line) ? Math.abs(line.end.x - line.start.x) : euclideanDistance(line).roundToInt()
}
function pointAtDistanceInBetween(line: Line, hopsFrom: number): Vector {
    let t: number = (hopsFrom * (euclideanDistance(line) / hops(line))) / euclideanDistance(line)
    let x: number = (1 - t) * line.start.x + t * line.end.x
    let y: number = (1 - t) * line.start.y + t * line.end.y
    return new Vector(x.roundToInt(), y.roundToInt())
}
function pointsInBetween(line: Line): Vector[] {
    return range(1, hops(line)).map(it => pointAtDistanceInBetween(line, it))
}
function euclideanDistance(line: Line): number {
    return Math.sqrt((line.end.x - line.start.x).pow(2) + (line.end.y - line.start.y).pow(2))
}
// Solution stolen from https://github.com/The-Self-Taught-Software-Engineer/advent-of-code-2021/blob/master/src/main/kotlin/codes/jakob/aoc/Day05.kt
// sorry i was very stuck XDD
// I'm bad at geometry
readFile("day-5/input.txt", "utf-8").then(fileContent => {
    return fileContent.split("\n").filter(str => str)
}).then(lines => {
    let vecLines: Line[] = []
    lines.forEach(line => {
        const [vec1Str, vec2Str] = line.split(" -> ")
        const [vec1X, vec1Y] = vec1Str.split(",").map(str => Number.parseInt(str))
        const [vec2X, vec2Y] = vec2Str.split(",").map(str => Number.parseInt(str))

        const vecLine: Line = {
            start: new Vector(vec1X, vec1Y),
            end: new Vector(vec2X, vec2Y)
        }
        vecLines.push(vecLine)

    });
    return vecLines
}).then((vecLines) => {
    let intersections = 0;
    function countOverlappingPoints(lines: Line[]): number {
        
        function count<K, V>(map: Map<K, V>, conditionFunc: (entry: [K, V]) => boolean): number {
            let count = 0;
            for (const entry of map) {
                if (conditionFunc(entry)) {
                    count++
                }
            }
            return count
        }
        let points: Vector[] = lines.flatMap((line) => [line.start, ...pointsInBetween(line), line.end])
        let pointsFrequencies: Map<number, Map<number, number>> = new Map()
        for(const point of points){
            if(!pointsFrequencies.has(point.y)){
                pointsFrequencies.set(point.y, new Map<number, number>())
            }
            let firstDim = pointsFrequencies.get(point.y);
            firstDim?.set(point.x, (firstDim.get(point.x) ?? 0)+1)
        }
        let pointsFrequenciesVector = new Map<Vector, number>();
        for(const entry of pointsFrequencies.entries()){
            for(const ent of entry[1].entries()){
                pointsFrequenciesVector.set(new Vector(ent[0], entry[0]), ent[1])
            }
        }
        let pos = new Vector(0,0);
        for(const i of range(pos.y+0, pos.x+10)){
            for(const j of range(pos.y + 0, pos.y+10)){
                const num = pointsFrequencies.get(i)?.get(j) ?? ".";
                process.stdout.write(" ")
                process.stdout.write(num.toString())
            }
            console.log()
        }
        return count(pointsFrequenciesVector, (entry) => entry[1] > 1)
    }
    intersections = countOverlappingPoints(vecLines)
    return intersections
}).then(intersections => console.log(intersections))