const pos = {
    x: 0,
    y: 0,
    aim: 0
}
const commands: {[command: string]: (arg: number) => void} = {
    forward: (x: number) => {
        pos.x += x
        pos.y += pos.aim * x
    },
    down: (y: number) => {
        pos.aim += y
    },
    up: (y: number) => {
        pos.aim -= y
    }
}
import { createReadStream } from "fs";
import * as readline from "readline"
const reportStream = createReadStream("day-2/instructions.txt", "utf-8")
let result = 0;
const rl = readline.createInterface({
    input: reportStream,
    crlfDelay: Infinity
});

rl.on('line', handleLine)
reportStream.on("end", () => {
    result = pos.x * pos.y;
    console.log("Result: "+ result)
})
function handleLine(line: string){
    const split = line.split(" ");
    commands[split[0]](Number.parseInt(split[1]))
}