import { readFile } from "fs/promises";
import { Game } from "../common";

readFile("day-4/input.txt", "utf-8").then((data) => {
    let game = new Game(data);
    game.getLastWinner().then(card => {
        let sum = 0
        card.numbers.forEach(line => line.forEach(num => {
            if(!num.marked){
                sum += num.num
            }
        }))
        console.log(game.lastRandomNumber*sum)
    }).catch(err => {
        console.error(err)
    })
})
