import { readFile } from "fs/promises";
import { Game } from "./common"
readFile("day-4/input.txt", "utf-8").then((data) => {
    let game = new Game(data);
    game.getWinner().then(card => {
        let sum = 0
        card.numbers.forEach(line => line.forEach(num => {
            if(!num.marked){
                sum += num.num
            }
        }))
        console.log(sum*game.lastRandomNumber)
    }).catch(err => {
        console.error(err)
    })
})
