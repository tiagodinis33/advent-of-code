import assert, { strictEqual } from "assert";

type CardNumber = { marked: boolean, num: number }
type RawCard = CardNumber[][]

export interface Card {
    won: boolean,
    numbers: RawCard
}
export class Game {
    randomNumbers: number[]
    cards: Card[]
    lastRandomNumber: number = Number.NaN
    constructor(data: string) {
        const lines = data.split("\n").filter(str => str)
        const firstLine = lines.shift()
        assert(firstLine)
        this.cards = splitArray(lines, 5)
            .map((card) => card
                .map((line) => line.split(" ").filter(str => str).map(num => {
                    return { marked: false, num: Number.parseInt(num) };
                }))).map((rawCard) => {
                    return { won: false, numbers: rawCard };
                })

        this.randomNumbers = firstLine.split(",").filter(str => str).map(str => Number.parseInt(str))
    }

    stepGame(num: number): Card | null {
        let lastWinner: Card | null = null;
        this.cards.forEach(card => {

            card.numbers.forEach(row => {
                row.filter(number => number.num == num).forEach((number, i) => {
                    if(!card.won){
                        number.marked = true
                        this.lastRandomNumber = num
                    }
                })

                for (let index = 0; index < 5; index++) {
                    if (
                        (// Check lines
                        card.numbers.every(row => row[index].marked) ||
                        // Check columns
                        card.numbers[index].every(row => row.marked)) && !card.won
                    ) {
                        lastWinner = card
                        card.won = true
                    }
                }
            })

        });
        return lastWinner
    }

    getLastWinner(): Promise<Card> {
        return new Promise((resolve, reject) => {
            let lastWinner: Card | null = null;
            let lastNum = 0
            for (const num of this.randomNumbers) {
                
                let winner = this.stepGame(num);
                if(winner) {
                    lastWinner = winner
                    console.log(num)
                }
            }
            if (lastWinner) {
                resolve(lastWinner)
                return;
            } else
                reject("No card won!")
        })
    }
    getWinner(): Promise<Card> {
        return new Promise((resolve, reject) => {
            for (const num of this.randomNumbers) {
                this.stepGame(num);
                const card = this.cards.find(card => card.won);
                if (card) {
                    this.lastRandomNumber = num
                    resolve(card)
                    return;
                }
            }
            if (!this.cards.some(card => card.won))
                reject("No card won!")
        })
    }
}
function splitArray<T>(arr: T[], size: number): T[][] {
    let result: T[][] = [];
    assert(!(arr.length % size))
    for (let i = 0; i < arr.length; i += size) {
        const el = arr[i];
        result.push(arr.slice(i, i + size));
    }
    return result;
}