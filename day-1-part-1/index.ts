import { createReadStream } from "fs";
import * as readline from "readline"
let lastNumber: number | undefined = undefined;
let largerMeasurementsCount = 0;
enum MeasurementType {
    NA,
    INCREASED,
    DECREASED
}
const reportStream = createReadStream("day-1-part-1/report.txt", "utf-8")

const rl = readline.createInterface({
    input: reportStream,
    crlfDelay: Infinity
});

rl.on('line', handleLine)
reportStream.on("end", () => {
    console.log(`Count: ${largerMeasurementsCount}`)
})
function handleLine(line: string){
    let currentNumber = Number.parseInt(line);
    let measurementType: MeasurementType | undefined = undefined;
    if(lastNumber == undefined){
        measurementType = MeasurementType.NA
    }else if(lastNumber > currentNumber){
        measurementType = MeasurementType.DECREASED
    } else if(lastNumber < currentNumber){
        measurementType = MeasurementType.INCREASED
    }
    switch(measurementType){
        case MeasurementType.NA:
        case MeasurementType.DECREASED:
            break;
        case MeasurementType.INCREASED:
            largerMeasurementsCount++;
    }
    lastNumber = currentNumber;
}